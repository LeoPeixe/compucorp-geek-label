(function($) {
  $(document).ready(function () {
    // Listen for resize changes and set the right hight
    SetCorrectHeight();
    window.addEventListener("resize", function() {
      // Get screen size (inner/outerWidth, inner/outerHeight)
      SetCorrectHeight();
    }, false);

    // Scroll Down to Others Blocks
    $('.btn-circle').on('click', function(e) {
      if (typeof $(this).data('to') != 'undefined' && $(this).data('to') != 'undefined' && $(this).data('to') != '') {
        var element = $('#block-block-'+$(this).data('to')+' .btn-circle');
        var parent = $('#block-block-'+$(this).data('to'));
        if ($(this).data('to') == 8) {
          $('body,html').animate({ scrollTop: $(document).height()}, 700);
        } else {
          $('body,html').animate({ scrollTop: element.offset().top - parent.height()}, 500);
        }
      }
    });

    // Light Slider
    // Clients
    $('#block-block-6 .content ul').lightSlider({
      item:3,
      loop:false,
      slideMove:1,
      speed:600,
      pager: false,
      controls: true,
      slideMargin: 0,
      prevHtml: '<i class="fa fa-chevron-left"></i>',
      nextHtml: '<i class="fa fa-chevron-right"></i>',
      responsive : [
      {
        breakpoint:800,
        settings: {
          item:2,
          slideMove:1,
          slideMargin:6,
        }
      },
      {
        breakpoint:480,
        settings: {
          item:1,
          pager: true,
          controls: false,
          slideMove:1
        }
      }
      ]
    });
  });
  function SetCorrectHeight() {
    var screen_size_w = $(window).width();
    var screen_size_h = $(window).height();
    screen_size_h = (screen_size_w/screen_size_h) >= 0.75 && screen_size_h < 600 ? 600 : $(window).height();
    $('body').append('<style type="text/css">\
      .region-content [id^="block-block"] {height:'+screen_size_h+'px;}\
      </style>');
    }
  })(jQuery);
