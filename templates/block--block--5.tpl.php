<?php
// Template for Services
?>

<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="container text-center">
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
      <h2><?php print $block->subject ?></h2>
    <?php endif;?>
    <?php print render($title_suffix); ?>

    <div class="content container"<?php print $content_attributes; ?>>
      <?php print $content ?>
    </div>
    <div class="btn-circle black" data-to="6">
      <i class="fas fa-chevron-down"></i>
    </div>
  </div>
</div>
