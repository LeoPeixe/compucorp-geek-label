<?php
// Template for How to find us
?>

<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="text-center">
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
      <h2><?php print $block->subject ?></h2>
    <?php endif;?>
    <?php print render($title_suffix); ?>

    <div class="content"<?php print $content_attributes; ?>>
      <?php print $content ?>
    </div>
    <div class="btn-circle red" data-to="8">
      <i class="fas fa-chevron-down"></i>
    </div>
  </div>
</div>
