<?php
// Template for Geek Label
?>

<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="container text-center">
    <?php 
    $logo = theme_get_setting('logo');;
    if ($logo): ?>
      <a href="/" title="<?php print t('Geek Label'); ?>" rel="home" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Geek Label'); ?>" />
      </a>
    <?php endif; ?>

      <div class="content"<?php print $content_attributes; ?>>
        <?php print $content ?>
      </div>
    <div class="btn-circle white" data-to="2">
      <i class="fas fa-chevron-down"></i>
    </div>
  </div>
</div>
