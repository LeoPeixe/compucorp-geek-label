<?php
// Template for Contact
?>

<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="container text-center">
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
      <h2><?php print $block->subject ?></h2>
    <?php endif;?>
    <?php print render($title_suffix); ?>

    <div class="content"<?php print $content_attributes; ?>>
      <form id="contact">
        <div class="form-group">
          <input type="text" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Name">
        </div>
        <div class="form-group">
          <input type="email" class="form-control" id="email" placeholder="Email">
        </div>
        <div class="form-group">
          <textarea class="form-control" id="message" rows="4" placeholder="Message"></textarea>
        </div>
        <button type="submit" class="btn-lg btn-primary">Send Message!</button>
      </form>
      <p>
        Or phone on: 01923 220121
      </p>
    </div>
  </div>
</div>
