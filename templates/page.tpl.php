<div id="page-wrapper"><div id="page">

  <div id="content"><div class="section">
    <?php print render($page['content']); ?>
  </div></div> <!-- /.section, /#content -->

</div></div> <!-- /#page, /#page-wrapper -->
